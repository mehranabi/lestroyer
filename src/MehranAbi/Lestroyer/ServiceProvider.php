<?php


namespace MehranAbi\Lestroyer;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    public function register()
    {
    }

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
        $this->publishes([
            __DIR__ . '/lestroyer.php' => config_path('lestroyer.php'),
        ]);
    }

}