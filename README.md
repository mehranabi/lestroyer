### Lestroyer

You can add this package to your Laravel project, and whenever you wanted to destroy that app without access to the server/hosting, you can simply send a GET request to a pre-defined URL, and this script will delete some critical files, so that the web app will be down until those files be added again.


### Installation
Use by adding this code to your `composer.json`:
```
...
"repositories": [
    {
        "type": "vcs",
        "url": "https://gitlab.com/mehranabi/lestroyer"
    }
],
...
require: {
    ...
    "mehranabi/lestroyer": "master",
    ...
},
...
```

#### Default Config
Destroy URL:
`/destroy/{code}`
which the `code` is defined in config file, default is `HolyCow!`.


### Developer
S. Mehran Abghari - mehran.ab80@gmail.com

### License
MIT
